variable "AUTHENTIK_TOKEN" {
  type = string
}

variable "DISCORD_CLIENT_ID" {
  type = string
}

variable "DISCORD_CLIENT_SECRET" {
  type = string
}

variable "DISCORD_GUILD_ID" {
  type = string
}
