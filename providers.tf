terraform {
  backend "http" {}
  required_providers {
    authentik = {
      source = "goauthentik/authentik"
      version = "2022.4.1"
    }
  }
}

provider "authentik" {
  url   = "https://auth.kylesferrazza.com"
  token = var.AUTHENTIK_TOKEN
}
