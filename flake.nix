{
  description = "authentik-config";
  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      name = "authentik-config";
      buildInputs = with pkgs; [
        terraform
        doppler
      ];
    };
  };
}
