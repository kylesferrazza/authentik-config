# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/goauthentik/authentik" {
  version     = "2022.4.1"
  constraints = "2022.4.1"
  hashes = [
    "h1:UMQYT0kLI8aw/oSdYkr1mreeDsU9ZPiVM4ulG/BNcS4=",
    "zh:2cd78e7f6e3ea0b50df6bdebf0df3a53f8e09bbdd90a414bcd3236723be7d82c",
    "zh:3e6b5ffa0e3f6aa5a9a88fe81ec585901628b99a54e2a778f76544ad23e5a4ea",
    "zh:3f1bc76bc34a31ec39572f647481cd6ea075f220dbb202b5b1952764ce560597",
    "zh:66e79b7b92e9c17b6e9a78a483b547159c3bada303e82e6ba10e33f367404e9c",
    "zh:72c4a2fc083dcc2d64dec12df78b7466466885d97a1f9432d2fd4fe8d3138191",
    "zh:895499d4efce42e2c4336d914d5a2a666553558bf2dd64cc7da96df406fe3ebe",
    "zh:92170a0473ada47451dac895a74f8c510c616d61d1b760254d2268b8e4a958a9",
    "zh:93a00b7d095bbd7f7ccfb3a1cf8cfdf95ddb649c653c2a071ed6786e9233c29e",
    "zh:a1481b23f2788ca4676232793be67c8ac857682b9ca84110da692fa01f7beb15",
    "zh:baf6615311b80ff724d2367a45c36536ff616c3d9f24c6e4ec4b2e15c072b2a5",
    "zh:bc3656f694faafbde4e16bda14ecc781bbccb2629036a284b464a26cfddba87f",
    "zh:d9431d34f6e27019f212bb736a392181d24fc2bddc8d91f2322b8887893a9356",
    "zh:ea879a3fca160c77a4ed92e047c15b3b30b845e10b568cbe203ea78474d0f020",
    "zh:fa2034bbc58e1d81bd9a292e52a83c5b2d7c3ca52cc7f59dc1384f0c2fd09b87",
  ]
}

provider "registry.terraform.io/kylesferrazza/authentik" {
  version     = "2022.1.31"
  constraints = "2022.1.31"
  hashes = [
    "h1:yJji3CLSoHNQfU7IeK+6/dBT6ELqEdaJ1ncy/7ZZpf4=",
    "zh:0a6cd181c1b09c91d086e26ff5ea69052733d18ea34db8b1d268afbe153e1617",
    "zh:19f89006480bac4fade301e355f68d627ece300cf071f20f94cc1d1a257608fa",
    "zh:1f318c0fe2736cc8cd75538e83833686d826f69e24b61d90717e7340bbea9be9",
    "zh:488c2678c24b747fb9608fda7f112aaba522e29f793b4475829afc75a988d1ce",
    "zh:49002beef77fc51e820b14d861409252e21deee5a83e2ce0b4acd9a3cf0de4d1",
    "zh:66fb3b32cc6aab6038205e33a5acc898e2c152cfea50166f64a51cac79ad9ad5",
    "zh:7c20f5ef1c4623272c9d0c8b5bd32765bc141085fabf50882c459722a92e90bb",
    "zh:8bd1cf1bf332f1e7d0963224155b6735c4e1fcef5c5d6d5d3682fd9529b3c31c",
    "zh:9640b77d9a4af456093a7c28a0b8cc8800764529e6414a85b784d19c9b31602a",
    "zh:cc34c95133e4d7205cd0fea8a9769ba9152426d25d0be5d9f4944970776b6b2a",
    "zh:de602530fb422253c656b27deb9c7f28c4b4539b4b08b311f988b8565f082b82",
    "zh:f2ae2dbea2f50e9d82b9f9d6d1b012ca8255e386a3c02e9d1ddc6f72cc7b2417",
    "zh:f7ad42728eb58c2f7e1922ae848f3597d5d92d9596628dc0caad0d46489b0662",
  ]
}
