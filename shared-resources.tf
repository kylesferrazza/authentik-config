resource "authentik_policy_binding" "password_change" {
  target = data.authentik_flow.default_password_change.id
  group  = data.authentik_group.admins.id
  order  = 0
}

resource "authentik_policy_expression" "source_auth_if_sso" {
  name = "source authentication if sso"
  expression = <<-EOF
    # This policy ensures that this flow can only be used when the user
    # is in a SSO Flow (meaning they come from an external IdP)
    return ak_is_sso_flow
  EOF
}

resource "authentik_stage_prompt_field" "username" {
  field_key = "username"
  label = "Username"
  type = "username"
}
