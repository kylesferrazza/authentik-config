#!/usr/bin/env bash
set -euo pipefail

TOKEN=$GITLAB_ACCESS_TOKEN
PROJ_ID="33255046"
STATE_NAME="prod"

terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/$PROJ_ID/terraform/state/$STATE_NAME" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/$PROJ_ID/terraform/state/$STATE_NAME/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/$PROJ_ID/terraform/state/$STATE_NAME/lock" \
    -backend-config="username=kylesferrazza" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
