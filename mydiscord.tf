resource "authentik_tenant" "discord" {
  domain = "discord-auth.kylesferrazza.com"
  branding_title = "Discord Auth"
  branding_logo = "https://brandlogos.net/wp-content/uploads/2021/11/discord-logo.png"
  branding_favicon = "https://brandlogos.net/wp-content/uploads/2021/11/discord-logo.png"
  flow_authentication = resource.authentik_flow.discord_default_auth_flow.uuid
}

resource "authentik_flow" "discord_default_auth_flow" {
  slug = "discord-default-auth-flow"
  name = "discord-default-auth-flow"
  title = "Discord Authentication"
  designation = "authentication"
  policy_engine_mode = "all"
}

resource "authentik_stage_identification" "discord_default_identification" {
  name           = "discord-default-identification"
  user_fields    = []
  sources        = [authentik_source_oauth.discord_source.uuid]
  case_insensitive_matching = true
  show_source_labels = true
}

resource "authentik_flow_stage_binding" "discord_default_ident" {
  target = resource.authentik_flow.discord_default_auth_flow.uuid
  stage = resource.authentik_stage_identification.discord_default_identification.id
  order = 10
  policy_engine_mode = "all"
}


resource "authentik_source_oauth" "discord_source" {
  name = "Discord"
  slug = "discord"
  authentication_flow = resource.authentik_flow.discord_source_authentication.uuid
  enrollment_flow = resource.authentik_flow.discord_source_enrollment.uuid

  provider_type = "discord"
  additional_scopes = "guilds identify email"

  consumer_key = var.DISCORD_CLIENT_ID
  consumer_secret = var.DISCORD_CLIENT_SECRET
}

resource "authentik_policy_expression" "deny_if_not_in_discord_guild" {
  name = "deny if not in discord guild (${var.DISCORD_GUILD_ID})"
  expression = <<-EOF
    # Guild ID can be obtained by turning on discord developer options and then right clicking on a guild icon and choosing Copy ID.
    accepted_guild_id = "${var.DISCORD_GUILD_ID}"

    connection = context['goauthentik.io/sources/connection']
    access_token = connection.access_token

    guilds = requests.get(
      "https://discord.com/api/users/@me/guilds",
      headers={
        "Authorization": "Bearer " + access_token
      },
    ).json()

    user_matched = any(guild['id'] == accepted_guild_id for guild in guilds)

    if not user_matched:
      ak_message("User is not member of the Discord guild.")

    return user_matched
  EOF
}

resource "authentik_flow" "discord_source_enrollment" {
  slug = "discord-source-enrollment"
  name = "discord-source-enrollment"
  title = "discord-source-enrollment"
  designation = "enrollment"
  policy_engine_mode = "all"
}

resource "authentik_stage_user_login" "discord_source_auth_login" {
  name = "discord-source-auth-login"
}

resource "authentik_flow" "discord_source_authentication" {
  slug = "discord-source-authentication"
  name = "discord-source-authentication"
  title = "discord-source-authentication"
  designation = "authentication"
  policy_engine_mode = "all"
}

resource "authentik_flow_stage_binding" "discord_source_auth_login" {
  target = resource.authentik_flow.discord_source_authentication.uuid
  stage = resource.authentik_stage_user_login.discord_source_auth_login.id
  order = 0
  policy_engine_mode = "all"
}

resource "authentik_policy_binding" "discord_source_auth_if_sso" {
  target = resource.authentik_flow.discord_source_authentication.uuid
  policy = resource.authentik_policy_expression.source_auth_if_sso.id
  order = 0
}

resource "authentik_policy_binding" "discord_source_enroll_if_sso" {
  target = resource.authentik_flow.discord_source_enrollment.uuid
  policy = resource.authentik_policy_expression.source_auth_if_sso.id
  order = 0
}

resource "authentik_policy_binding" "discord_auth_check_workspace" {
  target = resource.authentik_flow.discord_source_authentication.uuid
  policy = resource.authentik_policy_expression.deny_if_not_in_discord_guild.id
  order = 1
}

resource "authentik_policy_binding" "discord_enroll_check_workspace" {
  target = resource.authentik_flow.discord_source_enrollment.uuid
  policy = resource.authentik_policy_expression.deny_if_not_in_discord_guild.id
  order = 1
}

resource "authentik_group" "discord_users" {
  name = "discord-users"
  attributes = jsonencode({
    "goauthentik.io/user/can-change-name" = false
    "goauthentik.io/user/can-change-email" = false
    "goauthentik.io/user/can-change-username" = false
  })
}

resource "authentik_stage_prompt" "discord_source_prompt" {
  name = "discord-source-enrollment-prompt"
  fields = [
    resource.authentik_stage_prompt_field.username.id,
  ]
}

resource "authentik_flow_stage_binding" "discord_source_enroll_prompt" {
  target = resource.authentik_flow.discord_source_enrollment.uuid
  stage = resource.authentik_stage_prompt.discord_source_prompt.id
  order = 0
  policy_engine_mode = "all"
  re_evaluate_policies = true
}

resource "authentik_policy_expression" "discord_source_enrollment_set_username" {
  name = "discord-source-enrollment-set-username"
  expression = <<-EOF
    # Check if we've not been given a username by the external IdP
    # and trigger the enrollment flow
    context['prompt_data']['username'] = context['oauth_userinfo']['email']
    return 'username' not in context.get('prompt_data', {})
  EOF
}

resource "authentik_policy_binding" "discord_source_enroll_set_username" {
  target = resource.authentik_flow_stage_binding.discord_source_enroll_prompt.id
  policy = resource.authentik_policy_expression.discord_source_enrollment_set_username.id
  order = 0
}

resource "authentik_stage_user_write" "discord_source_enrollment_write" {
  name = "discord-source-enrollment-write"
  create_users_as_inactive = false
  create_users_group = resource.authentik_group.discord_users.id
}

resource "authentik_flow_stage_binding" "discord_source_enroll_write" {
  target = resource.authentik_flow.discord_source_enrollment.uuid
  stage = resource.authentik_stage_user_write.discord_source_enrollment_write.id
  order = 1
  policy_engine_mode = "all"
}

resource "authentik_stage_user_login" "discord_source_enrollment_login" {
  name = "discord-source-enrollment-login"
}

resource "authentik_flow_stage_binding" "discord_source_enroll_login" {
  target = resource.authentik_flow.discord_source_enrollment.uuid
  stage = resource.authentik_stage_user_login.discord_source_enrollment_login.id
  order = 2
  policy_engine_mode = "all"
}
